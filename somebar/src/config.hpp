// somebar - dwl bar
// See LICENSE file for copyright and license details.

#pragma once
#include "common.hpp"

constexpr bool topbar = true;

constexpr int paddingX = 10;
constexpr int paddingY = 3;

// See https://docs.gtk.org/Pango/type_func.FontDescription.from_string.html
constexpr const char* font = "Comic Code 14";

constexpr ColorScheme colorInactive = {Color(0xd7, 0x99, 0x21), Color(0x28, 0x28, 0x28)};
constexpr ColorScheme colorActive = {Color(0xff, 0xff, 0xff), Color(0xd7, 0x99, 0x21)};
constexpr const char* termcmd[] = {"foot", nullptr};

static std::vector<std::string> tagNames = {
	"O", "P", "E", "N", "B", "S", "D"
};

constexpr Button buttons[] = {
	{ ClkStatusText,   BTN_RIGHT,  spawn,      {.v = termcmd} },
};
